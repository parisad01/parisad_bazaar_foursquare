package com.bazaarinterview.parisad.dialogs.dialog_permission;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;

import com.bazaarinterview.parisad.databinding.LayoutPermissionDialogBinding;


public class DialogPermission {
    private final Activity activity;
    private final InterfaceDialogPermission interfaceDialogPermission;
    Dialog dialog;
    LayoutPermissionDialogBinding binding;

    public DialogPermission( Activity activity,
                            InterfaceDialogPermission interfaceDialogPermission) {
        this.activity = activity;
        this.interfaceDialogPermission=interfaceDialogPermission;
    }

    public void showDialog(){
        dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);

        LayoutInflater inflater = (LayoutInflater) activity.getApplicationContext().
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        binding = LayoutPermissionDialogBinding.inflate(inflater);
        View view = binding.getRoot();
        dialog.setContentView(view);
        //this part + dialog_corner.xml are for setting corner to dialog
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        //
        setListener();
        dialog.show();
   }
   //-----------------------------------------------------------------------------------------------
    private void setListener(){
        binding.addBtn.setOnClickListener(addBtnClick);
        binding.closeBtn.setOnClickListener(closeBtnClick);
    }
 //-------------------------------------------------------------------------------------------------
  View.OnClickListener addBtnClick=new View.OnClickListener() {
     @Override
     public void onClick(View view) {
         interfaceDialogPermission.startRequest(true);
         dialog.dismiss();
     }
 };
 //-------------------------------------------------------------------------------------------------
 View.OnClickListener closeBtnClick=new View.OnClickListener() {
     @Override
     public void onClick(View view) {
         interfaceDialogPermission.startRequest(false);
         dialog.dismiss();
     }
 };



}
