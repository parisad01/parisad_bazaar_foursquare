package com.bazaarinterview.parisad.roomDatabase.Place;

import android.os.AsyncTask;

import com.bazaarinterview.parisad.helper.G;
import com.bazaarinterview.parisad.roomDatabase.Detail.DetailDao;
import com.bazaarinterview.parisad.roomDatabase.Detail.DetailRoom;

import java.util.List;
import java.util.concurrent.ExecutionException;

//--------------------
public class PlaceRoomRepository {
    private static PlaceRoomRepository instance;
    private static PlaceDao myDao;
  //  static LiveData<List<LoginRoom>> mAll;
    static List<PlaceRoom> mAll;

    public static PlaceRoomRepository getInstance(){
        myDao = G.myRoomDataBase.PlaceDao();
       // mAll = loginDao.selectAll();
        if (instance==null){
            instance=new PlaceRoomRepository();
        }
        return instance;
    }


    public List<PlaceRoom> getAll() throws ExecutionException, InterruptedException {
        return new GetAllAsyncTask(myDao).execute().get();
    }

    public PlaceRoom getById(String id) throws ExecutionException, InterruptedException {
        return new GetByIdAsyncTask(myDao,id).execute().get();
    }

    public void insert (List<PlaceRoom> resultModel) {

        new insertAsyncTask(myDao).execute(resultModel);
    }
    public void deleteAll(){
        new DeleteAllAsyncTask(myDao).execute();
    }

    public Integer getCount() throws ExecutionException, InterruptedException {
         return new GetCountAsyncTask(myDao).execute().get();
    }

    private static class insertAsyncTask extends AsyncTask<List<PlaceRoom>, Void, Void> {
        private PlaceDao mAsyncTaskDao;
        insertAsyncTask(PlaceDao dao) {
            mAsyncTaskDao = dao;
        }
        @Override
        protected Void doInBackground(final List<PlaceRoom>... params) {
            mAsyncTaskDao.insert( params[0]);
            return null;
        }
    }
    //-----------------------------------------------------
    private static class DeleteAllAsyncTask extends AsyncTask<Void, Void, Void> {
        private PlaceDao mAsyncTaskDao;
        DeleteAllAsyncTask(PlaceDao dao){
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            mAsyncTaskDao.deleteAll();
            return null;
        }
    }
    //----------------------------------------------------
    private static class GetCountAsyncTask extends AsyncTask<Void, Void, Integer> {
        private PlaceDao mAsyncTaskDao;
        GetCountAsyncTask(PlaceDao dao){
            mAsyncTaskDao = dao;
        }
        @Override
        protected Integer doInBackground(Void... voids) {
            return mAsyncTaskDao.getCount();

        }
    }
  //-----------------------------------------------------
  private static class GetAllAsyncTask extends AsyncTask<Void, Void, List<PlaceRoom>> {
      private PlaceDao mAsyncTaskDao;
      GetAllAsyncTask(PlaceDao dao){
          mAsyncTaskDao = dao;
      }
      @Override
      protected List<PlaceRoom> doInBackground(Void... voids) {
          return mAsyncTaskDao.selectAll();

      }
  }
    //-----------------------------------------------------
    private static class GetByIdAsyncTask extends AsyncTask<Void, Void, PlaceRoom> {
        private PlaceDao mAsyncTaskDao;
        private String id;
        GetByIdAsyncTask(PlaceDao dao,String id){
            mAsyncTaskDao = dao;
            this.id=id;
        }
        @Override
        protected PlaceRoom doInBackground(Void... voids) {
            return mAsyncTaskDao.selectById( id);

        }
    }
    //-----------------------------------------------------
}
