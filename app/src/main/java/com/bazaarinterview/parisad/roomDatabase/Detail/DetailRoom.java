package com.bazaarinterview.parisad.roomDatabase.Detail;
//************this model is used for both retrofit and room*************//

import com.bazaarinterview.parisad.helper.Constant;
import com.bazaarinterview.parisad.roomDatabase.Place.PlaceRoom;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = Constant.table_detail)
public class DetailRoom extends PlaceRoom {
    @ColumnInfo(name = "canonical_url")
     private String canonicalUrl;
    @ColumnInfo(name = "url")
     private String url;
    @ColumnInfo(name = "photo_url")
    private String photoUrl;

    public String getCanonicalUrl() {
        return canonicalUrl;
    }

    public void setCanonicalUrl(String canonicalUrl) {
        this.canonicalUrl = canonicalUrl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }
}
