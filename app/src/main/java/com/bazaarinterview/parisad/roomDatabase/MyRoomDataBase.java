package com.bazaarinterview.parisad.roomDatabase;

import android.content.Context;
import android.os.Environment;

import com.bazaarinterview.parisad.roomDatabase.Detail.DetailDao;
import com.bazaarinterview.parisad.roomDatabase.Detail.DetailRoom;
import com.bazaarinterview.parisad.roomDatabase.LastLocation.LocationDao;
import com.bazaarinterview.parisad.roomDatabase.LastLocation.LocationRoom;
import com.bazaarinterview.parisad.roomDatabase.Place.PlaceDao;
import com.bazaarinterview.parisad.roomDatabase.Place.PlaceRoom;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;


//----------
@Database(entities = {PlaceRoom.class, LocationRoom.class, DetailRoom.class}
         , version = 1)
 public abstract class MyRoomDataBase extends RoomDatabase {
    public abstract PlaceDao PlaceDao();
    public abstract LocationDao LocationDao();
    public abstract DetailDao DetailDao();
    private static com.bazaarinterview.parisad.roomDatabase.MyRoomDataBase INSTANCE;

   public static com.bazaarinterview.parisad.roomDatabase.MyRoomDataBase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (com.bazaarinterview.parisad.roomDatabase.MyRoomDataBase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            com.bazaarinterview.parisad.roomDatabase.MyRoomDataBase.class,
                            "foursquare_db")
                            .addCallback(sRoomDatabaseCallback)
                            .build();

                }
            }
        }
        return INSTANCE;
    }

    private static Callback sRoomDatabaseCallback =
            new Callback(){

                @Override
                public void onOpen (@NonNull SupportSQLiteDatabase db){
                    super.onOpen(db);
                    new PopulateDbAsync(INSTANCE).execute();
                }
            };
}