package com.bazaarinterview.parisad.roomDatabase;


import android.os.AsyncTask;

import com.bazaarinterview.parisad.roomDatabase.Detail.DetailDao;
import com.bazaarinterview.parisad.roomDatabase.LastLocation.LocationDao;
import com.bazaarinterview.parisad.roomDatabase.Place.PlaceDao;


//----------------
public class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

    private final PlaceDao placeDao;
    private final LocationDao locationDao;
    private final DetailDao detailDao;

    PopulateDbAsync(MyRoomDataBase db) {

        placeDao = db.PlaceDao();
        locationDao=db.LocationDao();
        detailDao=db.DetailDao();
    }

    @Override
    protected Void doInBackground(final Void... params) {

        return null;
    }
}
