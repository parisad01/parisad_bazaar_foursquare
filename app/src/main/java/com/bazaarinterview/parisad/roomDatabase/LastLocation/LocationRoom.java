package com.bazaarinterview.parisad.roomDatabase.LastLocation;
//************this model is used for both retrofit and room*************//

import com.bazaarinterview.parisad.helper.Constant;
import com.google.gson.annotations.SerializedName;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = Constant.table_location)
public class LocationRoom {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "tableId")
    private int tableId;
    public int getTableId() {
        return tableId;
    }
    public void setTableId(int tableId) {
        this.tableId = tableId;
    }
    //-----------------------
    @ColumnInfo(name = "current_lat")
    @SerializedName("current_lat")
    private Double current_lat;

    @ColumnInfo(name = "current_lng")
    @SerializedName("current_lng")
    private Double current_lng;

    @ColumnInfo(name = "updated_on")
    @SerializedName("updated_on")
    private Long updated_on;

    public Double getCurrent_lat() {
        return current_lat;
    }

    public void setCurrent_lat(Double current_lat) {
        this.current_lat = current_lat;
    }

    public Double getCurrent_lng() {
        return current_lng;
    }

    public void setCurrent_lng(Double current_lng) {
        this.current_lng = current_lng;
    }

    public Long getUpdated_on() {
        return updated_on;
    }

    public void setUpdated_on(Long updated_on) {
        this.updated_on = updated_on;
    }

    public LocationRoom(Double current_lat, Double current_lng, Long updated_on) {
        this.current_lat = current_lat;
        this.current_lng = current_lng;
        this.updated_on = updated_on;
    }
}
