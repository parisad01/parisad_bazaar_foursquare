package com.bazaarinterview.parisad.roomDatabase.Detail;

import android.os.AsyncTask;

import com.bazaarinterview.parisad.helper.G;
import com.bazaarinterview.parisad.roomDatabase.Place.PlaceDao;
import com.bazaarinterview.parisad.roomDatabase.Place.PlaceRoom;

import java.util.List;
import java.util.concurrent.ExecutionException;

//--------------------
public class DetailRoomRepository {
    private static DetailRoomRepository instance;
    private static DetailDao myDao;

    public static DetailRoomRepository getInstance(){
        myDao = G.myRoomDataBase.DetailDao();
       // mAll = loginDao.selectAll();
        if (instance==null){
            instance=new DetailRoomRepository();
        }
        return instance;
    }


    public List<DetailRoom> getAll() throws ExecutionException, InterruptedException {
        return new GetAllAsyncTask(myDao).execute().get();
    }

    public DetailRoom getById(String id) throws ExecutionException, InterruptedException {
        return new GetByIdAsyncTask(myDao,id).execute().get();
    }

    public void insert (DetailRoom resultModel) {
        new insertAsyncTask(myDao).execute(resultModel);
    }
    public void deleteAll(){
        new DeleteAllAsyncTask(myDao).execute();
    }

    public Integer getCount() throws ExecutionException, InterruptedException {
         return new GetCountAsyncTask(myDao).execute().get();
    }

    private static class insertAsyncTask extends AsyncTask<DetailRoom, Void, Void> {
        private DetailDao mAsyncTaskDao;
        insertAsyncTask(DetailDao dao) {
            mAsyncTaskDao = dao;
        }
        @Override
        protected Void doInBackground(final DetailRoom ... params) {
            mAsyncTaskDao.insert( params[0]);
            return null;
        }
    }
    //-----------------------------------------------------
    private static class DeleteAllAsyncTask extends AsyncTask<Void, Void, Void> {
        private DetailDao mAsyncTaskDao;
        DeleteAllAsyncTask(DetailDao dao){
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            mAsyncTaskDao.deleteAll();
            return null;
        }
    }
    //----------------------------------------------------
    private static class GetCountAsyncTask extends AsyncTask<Void, Void, Integer> {
        private DetailDao mAsyncTaskDao;
        GetCountAsyncTask(DetailDao dao){
            mAsyncTaskDao = dao;
        }
        @Override
        protected Integer doInBackground(Void... voids) {
            return mAsyncTaskDao.getCount();

        }
    }
  //-----------------------------------------------------
  private static class GetAllAsyncTask extends AsyncTask<Void, Void, List<DetailRoom>> {
      private DetailDao mAsyncTaskDao;
      GetAllAsyncTask(DetailDao dao){
          mAsyncTaskDao = dao;
      }
      @Override
      protected List<DetailRoom> doInBackground(Void... voids) {
          return mAsyncTaskDao.selectAll();

      }
  }

    //-----------------------------------------------------
    private static class GetByIdAsyncTask extends AsyncTask<Void, Void, DetailRoom> {
        private DetailDao mAsyncTaskDao;
        private String id;
        GetByIdAsyncTask(DetailDao dao,String id){
            mAsyncTaskDao = dao;
            this.id=id;
        }
        @Override
        protected DetailRoom doInBackground(Void... voids) {
            return mAsyncTaskDao.selectById( id);

        }
    }
    //-----------------------------------------------------
}
