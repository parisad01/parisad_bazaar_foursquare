package com.bazaarinterview.parisad.roomDatabase.Detail;

import com.bazaarinterview.parisad.helper.Constant;
import com.bazaarinterview.parisad.roomDatabase.Place.PlaceRoom;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

@Dao
public interface DetailDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(DetailRoom detailRoom);

    @Query("DELETE FROM "+ Constant.table_detail)
    void deleteAll();

    @Query("SELECT * FROM "+Constant.table_detail)
    List<DetailRoom> selectAll();

    @Query("SELECT COUNT(*) FROM "+Constant.table_detail)
    int getCount();

    @Query("SELECT * FROM "+Constant.table_detail+" WHERE id= :venueId")
    DetailRoom selectById(String venueId);

}
