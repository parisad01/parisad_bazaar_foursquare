package com.bazaarinterview.parisad.roomDatabase.LastLocation;

import com.bazaarinterview.parisad.helper.Constant;
import com.bazaarinterview.parisad.roomDatabase.Place.PlaceRoom;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

@Dao
public interface LocationDao {
    @Insert
    void insert(LocationRoom[] locationRoom);

    @Query("DELETE FROM "+ Constant.table_location)
    void deleteAll();

    @Query("SELECT * FROM "+Constant.table_location)
    List<LocationRoom> selectAll();

    @Query("SELECT COUNT(*) FROM "+Constant.table_location)
    int getCount();
}
