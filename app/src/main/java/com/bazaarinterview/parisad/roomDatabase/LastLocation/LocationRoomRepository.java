package com.bazaarinterview.parisad.roomDatabase.LastLocation;

import android.os.AsyncTask;

import com.bazaarinterview.parisad.helper.G;

import java.util.List;
import java.util.concurrent.ExecutionException;

import androidx.room.Dao;

//--------------------
public class LocationRoomRepository {
    private static LocationRoomRepository instance;
    private static LocationDao myDao;
  //  static LiveData<List<LoginRoom>> mAll;
    static List<LocationRoom> mAll;

    public static LocationRoomRepository getInstance(){
        myDao = G.myRoomDataBase.LocationDao();
       // mAll = loginDao.selectAll();
        if (instance==null){
            instance=new LocationRoomRepository();
        }
        return instance;
    }


    public List<LocationRoom> getAll() throws ExecutionException, InterruptedException {
//        mAll = loginDao.selectAll();
//        return mAll;
        return new GetAllAsyncTask(myDao).execute().get();
    }
    public void insert (LocationRoom resultModel) {
        new insertAsyncTask(myDao).execute(resultModel);
    }
    public void deleteAll(){
        new DeleteAllAsyncTask(myDao).execute();
    }

    public Integer getCount() throws ExecutionException, InterruptedException {
         return new GetCountAsyncTask(myDao).execute().get();
    }

    private static class insertAsyncTask extends AsyncTask<LocationRoom, Void, Void> {
        private LocationDao mAsyncTaskDao;
        insertAsyncTask(LocationDao dao) {
            mAsyncTaskDao = dao;
        }
        @Override
        protected Void doInBackground(final LocationRoom... params) {
            mAsyncTaskDao.insert( params);
            return null;
        }
    }
    //-----------------------------------------------------
    private static class DeleteAllAsyncTask extends AsyncTask<Void, Void, Void> {
        private LocationDao mAsyncTaskDao;
        DeleteAllAsyncTask(LocationDao dao){
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            mAsyncTaskDao.deleteAll();
            return null;
        }
    }
    //----------------------------------------------------
    private static class GetCountAsyncTask extends AsyncTask<Void, Void, Integer> {
        private LocationDao mAsyncTaskDao;
        GetCountAsyncTask(LocationDao dao){
            mAsyncTaskDao = dao;
        }
        @Override
        protected Integer doInBackground(Void... voids) {
            return mAsyncTaskDao.getCount();

        }
    }
  //-----------------------------------------------------
  private static class GetAllAsyncTask extends AsyncTask<Void, Void, List<LocationRoom>> {
      private LocationDao mAsyncTaskDao;
      GetAllAsyncTask(LocationDao dao){
          mAsyncTaskDao = dao;
      }
      @Override
      protected List<LocationRoom> doInBackground(Void... voids) {
          return mAsyncTaskDao.selectAll();

      }
  }
    //-----------------------------------------------------
}
