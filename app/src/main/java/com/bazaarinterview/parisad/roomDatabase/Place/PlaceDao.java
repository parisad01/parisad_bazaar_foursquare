package com.bazaarinterview.parisad.roomDatabase.Place;

import com.bazaarinterview.parisad.helper.Constant;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

@Dao
public interface PlaceDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<PlaceRoom> placeRooms);

    @Query("DELETE FROM "+ Constant.table_place)
    void deleteAll();

    @Query("SELECT * FROM "+Constant.table_place)
    List<PlaceRoom> selectAll();

    @Query("SELECT COUNT(*) FROM "+Constant.table_place)
    int getCount();

    @Query("SELECT * FROM "+Constant.table_place+" WHERE id= :venueId")
    PlaceRoom selectById(String venueId);

}
