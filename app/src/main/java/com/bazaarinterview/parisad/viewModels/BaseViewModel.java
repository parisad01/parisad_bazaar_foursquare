package com.bazaarinterview.parisad.viewModels;

import android.content.Context;
import android.location.Location;

import com.bazaarinterview.parisad.helper.Constant;
import com.bazaarinterview.parisad.helper.GPS_listener;
import com.bazaarinterview.parisad.helper.Utilities;
import com.bazaarinterview.parisad.roomDatabase.LastLocation.LocationRoom;
import com.bazaarinterview.parisad.roomDatabase.LastLocation.LocationRoomRepository;

import java.util.Date;
import java.util.concurrent.ExecutionException;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class BaseViewModel extends ViewModel {
    public MutableLiveData<Boolean> isGPS_on=new MutableLiveData<>();
    public MutableLiveData<Boolean> isOnline=new MutableLiveData<>();
    public MutableLiveData<Boolean> isShowLoading=new MutableLiveData<>();
    public MutableLiveData<Boolean> isGPS_clicked=new MutableLiveData<>();
    public MutableLiveData<Boolean> isWIFI_clicked=new MutableLiveData<>();
    LocationRoomRepository locationRoomRepository;

    public void init(Context context){
        locationRoomRepository=LocationRoomRepository.getInstance();
    }

    public boolean  isDataOld() throws ExecutionException, InterruptedException {
        if (dao_getCountLocation()>0){
            LocationRoom locationRoom=dao_getLastLocation();
            Date date=new Date();
            if (date.getTime()-locationRoom.getUpdated_on()>= Constant.END_TIME){
                return true;
            }
            return false;
        }
        return false;
    }
    public LocationRoom dao_getLastLocation() throws ExecutionException, InterruptedException {
        if (dao_getCountLocation()>0){
            return locationRoomRepository.getAll().get(0);
        }
        return null;
    }
    public void check_gps(Context context){
        Utilities utilities=new Utilities();
        isGPS_on.setValue(utilities.isGPS_ON(context));
    }
    public void check_net(Context context){
        Utilities utilities=new Utilities();
        isOnline.setValue(utilities.isOnline(context));
    }
    public int dao_getCountLocation() throws ExecutionException, InterruptedException {
        return locationRoomRepository.getCount();
    }
    public void showLoading(){
        isShowLoading.setValue(true);
    }
    public void notShowLoading(){
        isShowLoading.setValue(false);
    }

    public Location getMyLocation(Context context){
        GPS_listener gps_listener=new GPS_listener(context);
        return gps_listener.getMyLocation();

    }

    public MutableLiveData<Boolean> getIsGPS_on() {
        return isGPS_on;
    }

    public MutableLiveData<Boolean> getIsOnline() {
        return isOnline;
    }

    public void gps_click(){
        isGPS_clicked.setValue(true);
    }

    public void wifi_click(){
        isWIFI_clicked.setValue(true);
    }

    public MutableLiveData<Boolean> getIsGPS_clicked() {
        return isGPS_clicked;
    }

    public MutableLiveData<Boolean> getIsWIFI_clicked() {
        return isWIFI_clicked;
    }

}
