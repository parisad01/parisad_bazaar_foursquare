package com.bazaarinterview.parisad.viewModels;

import android.content.Context;
import android.location.Location;
import android.util.Log;

import com.bazaarinterview.parisad.helper.Constant;
import com.bazaarinterview.parisad.interfaces.NavigatorInterface;
import com.bazaarinterview.parisad.mappers.Mapper;
import com.bazaarinterview.parisad.models.PlaceModel;
import com.bazaarinterview.parisad.models.RequestOutput;
import com.bazaarinterview.parisad.repositories.PlaceRepository;
import com.bazaarinterview.parisad.retrofits.Place.GetPlaceResponseInterface;
import com.bazaarinterview.parisad.roomDatabase.LastLocation.LocationRoom;
import com.bazaarinterview.parisad.roomDatabase.Place.PlaceRoom;
import com.bazaarinterview.parisad.roomDatabase.Place.PlaceRoomRepository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class MainActivityViewModel extends BaseViewModel {
    public MutableLiveData<String> userMessage=new MutableLiveData<>();
 //   public MutableLiveData<Boolean> isDataChanged=new MutableLiveData<>();
    public MutableLiveData<List<PlaceModel>> placeList=new MutableLiveData<>();
    PlaceRepository placeRepository;
    PlaceRoomRepository placeRoomRepository;
    Location lastLocation,currentLocation;
    private NavigatorInterface navigatorInterface;
    public void setNavigatorInterface(NavigatorInterface navigatorInterface){
        this.navigatorInterface=navigatorInterface;
    }
     //--------------------------------------------------------

    @Override
    public void init(Context context) {
        super.init(context);
        placeRepository=PlaceRepository.getInstance();
        placeRoomRepository=PlaceRoomRepository.getInstance();
        isShowLoading.setValue(false);
        try {
            getPlaces(context);
        }catch (Exception ex){
            String str=ex.getMessage();
        }
    }

    //---------------------------------------------------------
    private void getPlaces(Context context) throws ExecutionException, InterruptedException {
        //gps on
        try{
        if (isGPS_on.getValue()){
            if (isLocationChanged() ){//|| dao_getCount()==0
                if (isOnline.getValue()) {
                    dao_deleteAllPlaces();
                    getDataFromServer(context);
                }else{
                    getDataFromDB(context,isGPS_on.getValue());
                }
            }else{
                getDataFromDB(context,isGPS_on.getValue());
            }
            //gps off
        }else{
            getDataFromDB(context,isGPS_on.getValue());
        }
        }catch (Exception ex){
           String str=ex.getMessage();
        }
    }
    //---------------------------------------------------------
    public void getDataFromServer(Context context){
        try {
            if (isOnline.getValue()) {
                showLoading();
                Integer v = Constant.v;
                Integer limit = 20;
                Integer offset = setOffset(limit);
                currentLocation = getMyLocation(context);
                if (currentLocation != null) {
                    String ll = currentLocation.getLatitude() + "," + currentLocation.getLongitude();
                    String query = "";
                    placeRepository.getPlaces(getPlaceResponseInterface,
                            Constant.CLIENT_ID, Constant.CLIENT_SECRET,
                            v, limit, offset, ll, query);
                } else {
                    notShowLoading();
                    userMessage.setValue("no location find, try again");
                }
            } else {
                userMessage.setValue("no internet");
            }
        }catch (Exception ex){
            userMessage.setValue(ex.getMessage()+"getDataFromServer+mainvm");
        }
    }
    //---------------------------------------------------------
    private void getDataFromDB(Context context,boolean isGPSOn) throws ExecutionException, InterruptedException {
       if(isGPSOn){
           if (isDataOld()) {
               userMessage.setValue("data is old ");
               if (isOnline.getValue()){
                   dao_deleteAllPlaces();
                   getDataFromServer(context);
               }else{
                   setDataList();
               }

           }else{
               setDataList();
           }
       }else {
           if (dao_getCount() == 0) {
               userMessage.setValue("no offline data");
           } else {
               if (isDataOld()) {
                   userMessage.setValue("data is old ");
               }
               setDataList();
           }
       }
    }

    //---------------------------------------------------------
    private boolean isLocationChanged() throws ExecutionException, InterruptedException {
        LocationRoom locationRoom;
       try{
         locationRoom=dao_getLastLocation();
       }catch (Exception ex){
           String str=ex.getMessage();
           Log.e("TAG", "isLocationChanged: ",ex );
           locationRoom=null;
       }
        if (locationRoom==null){//db is empty so must get data from server
            return true;
        }else {
            lastLocation=new Location("");
            lastLocation.setLatitude(locationRoom.getCurrent_lat());
            lastLocation.setLongitude(locationRoom.getCurrent_lng());
            Double distance = calculateDistance(lastLocation,currentLocation);
            if (distance > Constant.END_DISTANCE) {
                return true;
            } else {
                return false;
            }
        }
    }
    //---------------------------------------------------------
    public void loadDataMore(int position,Context context){
        if (position>=placeList.getValue().size()-5 && !isShowLoading.getValue())
        {
            getDataFromServer(context);
        }
    }
    //---------------------------------------------------------
    private int dao_getCount() throws ExecutionException, InterruptedException {
        int i= placeRoomRepository.getCount();
        return i;
    }
    //--------------------------------------------------------
    private List<PlaceRoom> dao_getAllPlace() throws ExecutionException, InterruptedException {
        return placeRoomRepository.getAll();
    }
    //--------------------------------------------------------
    private void dao_saveList(){
        Mapper mapper=new Mapper();
        placeRoomRepository.insert(mapper.mapToEntity_list(placeList.getValue()));
    }
    //--------------------------------------------------------
    private void dao_deleteAllPlaces(){
        placeRoomRepository.deleteAll();
    }
    //--------------------------------------------------------
    private void dao_saveLastLocation(Location location){
       LocationRoom locationRoom=new LocationRoom(location.getLatitude(),
                                                  location.getLongitude(),
                                                   new Date().getTime());
       locationRoomRepository.deleteAll();///???????
       locationRoomRepository.insert(locationRoom);
    }
    //--------------------------------------------------------
    public LiveData<String> getUserMessage(){
        return userMessage;
    }
    //--------------------------------------------------------
    public void setDataList() throws ExecutionException, InterruptedException {
       Mapper mapper=new Mapper();
       placeList.setValue(mapper.mapToModel_list(dao_getAllPlace()));
       //isDataChanged.setValue(true);
    }
    //--------------------------------------------------------

    private Double calculateDistance(Location loc1,Location loc2){
        Double distance=0.0;
        if (loc1!=null && loc2 !=null) {
           distance=Math.sqrt(Math.pow(lastLocation.getLatitude() - currentLocation.getLatitude(), 2) +
                    Math.pow(lastLocation.getLongitude() - currentLocation.getLongitude(), 2));
        }
        return distance;
    }
    //--------------------------------------------------------
    //calculate offset for load more data
    private Integer setOffset(Integer limit){
        Integer r=0;
        List<PlaceModel> placeModels=placeList.getValue();
        if (placeModels!=null && placeModels.size()>0){
            r=placeModels.size();
        }
        return r;
    }

    public LiveData<List<PlaceModel>> getPlaceList(){
        return placeList;
    }

    //-------------------------------------------------------------------
    public void showDetail(PlaceModel model){
             //go to detail activity
        navigatorInterface.onItemClick(model);
    }
    //-------------------------------------------------------------------
    GetPlaceResponseInterface getPlaceResponseInterface=new GetPlaceResponseInterface() {
        @Override
        public void updateUI(boolean response, String str, RequestOutput requestOutput) throws ExecutionException, InterruptedException {
            notShowLoading();
            //save to database
            if (response && requestOutput!=null) {
                List<PlaceModel> places=new Mapper().ReqOutToPlaceModel(requestOutput);
                List<PlaceModel> old_pr = placeList.getValue();
                if (old_pr==null){
                    old_pr=new ArrayList<>();
                }
                old_pr.addAll(places);
                placeList.setValue(old_pr);

                dao_saveList();
                setDataList();
                dao_saveLastLocation(currentLocation);
            }else{
                userMessage.setValue("no data from server");
            }
        }
    };
    //---------------------------------------------------------------
}
