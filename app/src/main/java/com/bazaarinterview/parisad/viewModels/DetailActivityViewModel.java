package com.bazaarinterview.parisad.viewModels;

import android.content.Context;

import com.bazaarinterview.parisad.helper.Constant;
import com.bazaarinterview.parisad.helper.Utilities;
import com.bazaarinterview.parisad.interfaces.NavigatorInterface;
import com.bazaarinterview.parisad.interfaces.OpenUrlInterface;
import com.bazaarinterview.parisad.mappers.Mapper;
import com.bazaarinterview.parisad.models.DetailModel;
import com.bazaarinterview.parisad.models.DetailRequestOutput;
import com.bazaarinterview.parisad.models.PlaceModel;
import com.bazaarinterview.parisad.repositories.DetailRepository;
import com.bazaarinterview.parisad.retrofits.Detail.GetDetailResponseInterface;
import com.bazaarinterview.parisad.roomDatabase.Detail.DetailRoom;
import com.bazaarinterview.parisad.roomDatabase.Detail.DetailRoomRepository;
import com.bazaarinterview.parisad.roomDatabase.LastLocation.LocationRoom;
import com.bazaarinterview.parisad.roomDatabase.LastLocation.LocationRoomRepository;
import com.bazaarinterview.parisad.roomDatabase.Place.PlaceRoom;
import com.bazaarinterview.parisad.roomDatabase.Place.PlaceRoomRepository;

import java.util.Date;
import java.util.concurrent.ExecutionException;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class DetailActivityViewModel extends BaseViewModel {
//    public MutableLiveData<Boolean> isGPS_on=new MutableLiveData<>();
//    public MutableLiveData<Boolean> isOnline=new MutableLiveData<>();
//    public MutableLiveData<Boolean> isShowLoading=new MutableLiveData<>();
    public MutableLiveData<String> userMessage=new MutableLiveData<>();
    public MutableLiveData<DetailModel> detailModel=new MutableLiveData<>();
    public MutableLiveData<String> name=new MutableLiveData<>();
    public MutableLiveData<String> address=new MutableLiveData<>();
    public MutableLiveData<String> distance=new MutableLiveData<>();
    public MutableLiveData<String> url=new MutableLiveData<>();
    public MutableLiveData<String> photoUrl=new MutableLiveData<>();
    public MutableLiveData<String> iconUrl=new MutableLiveData<>();
    public MutableLiveData<String> nameFA=new MutableLiveData<>();
    public MutableLiveData<String> nameEN=new MutableLiveData<>();
    DetailRepository detailRepository;
    DetailRoomRepository detailRoomRepository;
    LocationRoomRepository locationRoomRepository;
    PlaceRoomRepository placeRoomRepository;
    LocationRoom currentLocation;
    private String venueId;
    public void setVenueId(String id){
        this.venueId=id;
    }
    private OpenUrlInterface myInterface;
    public void setOpenUrlInterface(OpenUrlInterface myInterface){
        this.myInterface=myInterface;
    }

    @Override
    public void init(Context context) {
        super.init(context);
        detailRepository=DetailRepository.getInstance();
        //  locationRoomRepository=LocationRoomRepository.getInstance();
        detailRepository=DetailRepository.getInstance();
        detailRoomRepository=DetailRoomRepository.getInstance();
        placeRoomRepository=PlaceRoomRepository.getInstance();
        getDetail(context);
    }


    public void getDetail(Context context){
        try{
            if (isOnline.getValue()){
                if (!isDataOld() && dao_getById(venueId)!=null){
                    getDataFromDB(venueId);
                }else{
                    //at first get data from places
                    PlaceRoom placeRoom=dao_getById_place(venueId);
                    showData_place(placeRoom);
                    getDataFromServer(context);
                }
            }else{
                getDataFromDB(venueId);
            }

        }catch (Exception ex){

        }

    }

    public void getDataFromServer(Context context) throws ExecutionException, InterruptedException {
        if (isOnline.getValue()) {
            showLoading();
            Integer v = Constant.v;
            currentLocation=dao_getLastLocation();
            if (currentLocation != null) {
                String ll = currentLocation.getCurrent_lat() +
                        "," +
                        currentLocation.getCurrent_lng();
                String query = "";
                detailRepository.getDetail(getDetailResponseInterface,venueId,
                        Constant.CLIENT_ID, Constant.CLIENT_SECRET,
                        v, ll, query);
            } else {
                notShowLoading();
                userMessage.setValue("no location find");
            }
        }else{
            userMessage.setValue("no internet");
        }
    }

    public void getDataFromDB(String id) throws ExecutionException, InterruptedException {
         DetailRoom detailRoom=dao_getById(id);
         if (detailRoom!=null) {
             DetailModel model = new Mapper().mapToDetailModel(detailRoom);
             showData(model);
         }else{
             PlaceRoom placeRoom=dao_getById_place(id);
             showData_place(placeRoom);
         }
    }

    public void showData_place(PlaceRoom model){
        if (model!=null) {
            DetailModel dm=new DetailModel();
            dm=new Mapper().PlaceRoomToDetailModel(model);
            detailModel.setValue(dm);
            name.setValue(model.getName());
            address.setValue(model.getAddress());
            url.setValue(Constant.EMPTY_URL);

            String km=new Utilities().meterTokilo(model.getDistance());
            distance.setValue(km+" "+Constant.KILOMETER);
            String[] names=getNames(model.getName());
            if (names!=null ) {
                nameEN.setValue((names.length >= 1) ? names[0] : "");
                nameFA.setValue((names.length >= 2) ? names[1] : "");
            }
            iconUrl.setValue(model.getCategory_icon());
            photoUrl.setValue("");

        }else{
            userMessage.setValue("no data find");
        }
    }

    //show data in ui
    public void showData(DetailModel model){
        if (model!=null) {
            detailModel.setValue(model);
            name.setValue(model.getName());
            address.setValue(model.getAddress());
            if (model.getUrl()==null || model.getUrl().isEmpty()){
                url.setValue(Constant.EMPTY_URL);
            }else{
                url.setValue(model.getUrl());
            }

            String km=new Utilities().meterTokilo(model.getDistance());
            distance.setValue(km+" "+Constant.KILOMETER);
            String[] names=getNames(model.getName());
            if (names!=null ) {
                nameEN.setValue((names.length >= 1) ? names[0] : "");
                nameFA.setValue((names.length >= 2) ? names[1] : "");
            }
            iconUrl.setValue(model.getCategory_icon());
            photoUrl.setValue(model.getPhotoUrl());

        }else{
            userMessage.setValue("no data find");
        }
    }

    private String[] getNames(String name){
        return name.split("\\|");
    }

    public LiveData<String> getPhotoUrl(){
        return photoUrl;
    }
    public LiveData<String> getIconUrl(){
        return iconUrl;
    }

    public void urlClick(){
        try {
            if (!url.getValue().isEmpty() &&
                    !url.getValue().equals(Constant.EMPTY_URL)) {
                myInterface.openUrl(url.getValue());
            }
        }catch(Exception ex){
            userMessage.setValue("به هنگام باز کردن صفحه مشکلی پیش آمده است");
        }
    }

    public void canonicaUrlClick(){
        try {
            String canUrl = detailModel.getValue().getCanonicalUrl();
            if (!canUrl.isEmpty()) {
                myInterface.openUrl(canUrl);
            }
        }catch (Exception ex){
            userMessage.setValue("به هنگام باز کردن صفحه مشکلی پیش آمده است");
        }
    }

    private DetailRoom dao_getById(String id) throws ExecutionException, InterruptedException {
        return detailRoomRepository.getById(id);
    }

    private PlaceRoom dao_getById_place(String id) throws ExecutionException, InterruptedException {
        return placeRoomRepository.getById(id);
    }


    private void dao_insertDetail(DetailRoom room){
        try {
            detailRoomRepository.insert(room);
        }catch(Exception ex){
            String str=ex.getMessage();
        }
    }

    public LiveData<String> getUserMessage(){
        return userMessage;
    }

    GetDetailResponseInterface getDetailResponseInterface=new GetDetailResponseInterface() {
        @Override
        public void updateUI(boolean response, String str, DetailRequestOutput detailOutput) throws ExecutionException, InterruptedException {
            notShowLoading();
            if (response && detailOutput!=null){
                DetailModel model=new Mapper().ReqOutToDetailModel(detailOutput);
                if (model!=null) {
                    dao_insertDetail(new Mapper().mapToDetailRoom(model));
                    showData(model);
                }

            }else{
                userMessage.setValue(str);
            }
        }
    };
}
