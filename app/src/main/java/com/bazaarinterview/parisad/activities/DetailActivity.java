package com.bazaarinterview.parisad.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.os.Build;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bazaarinterview.parisad.R;
import com.bazaarinterview.parisad.bases.BaseVMActivity;
import com.bazaarinterview.parisad.databinding.ActivityDetailBinding;
import com.bazaarinterview.parisad.databinding.ActivityMainBinding;
import com.bazaarinterview.parisad.helper.Constant;
import com.bazaarinterview.parisad.helper.Utilities;
import com.bazaarinterview.parisad.interfaces.OpenUrlInterface;
import com.bazaarinterview.parisad.viewModels.DetailActivityViewModel;
import com.bazaarinterview.parisad.viewModels.MainActivityViewModel;
import com.squareup.picasso.Picasso;

public class DetailActivity extends BaseVMActivity implements OpenUrlInterface {
    DetailActivityViewModel viewModel;
    ActivityDetailBinding myBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        viewModel = ViewModelProviders.of(this).get(DetailActivityViewModel.class);
        myBinding= (ActivityDetailBinding) binding(R.layout.activity_detail, viewModel);

           viewModel.setOpenUrlInterface(this);
        setObserver();
    }

    @Override
    protected void onResume() {
        super.onResume();
        viewModel.check_gps(getApplicationContext());
        viewModel.check_net(getApplicationContext());
        viewModel.setVenueId(getIntent().getStringExtra(Constant.EXTRA_ID));
        viewModel.init(getApplicationContext());


    }



    public void setObserver(){
        viewModel.getPhotoUrl().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                if (!s.isEmpty()){
                    try {
                        Picasso.get().load(s).fit().centerCrop()
                                .placeholder(R.drawable.ic_slice9)
                                .error(R.drawable.ic_slice9)
                                .into(myBinding.imageView);
                    }catch (Exception ex){
                        String str=ex.getMessage();
                    }

                }
            }
        });
        //------------------------
        viewModel.getIconUrl().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                try {
                    if (!s.isEmpty()) {
                        Picasso.get().load(s).fit().centerCrop()
                                .placeholder(R.drawable.ic_no_category)
                                .error(R.drawable.ic_no_category)
                                .into(myBinding.categoryIcon);

                    }
                }catch(Exception ex){
                    String str=ex.getMessage();
                }
            }
        });
        //------------------------
        viewModel.getUserMessage().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                if (!s.isEmpty())
                Toast.makeText(getApplicationContext(),s,Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void openUrl(String url) {
        try {
            new Utilities().openUrl(url, getApplicationContext());
        }catch (Exception ex){

        }
    }


}