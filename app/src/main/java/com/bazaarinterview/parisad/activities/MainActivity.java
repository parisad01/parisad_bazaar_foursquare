package com.bazaarinterview.parisad.activities;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.widget.Toast;

import com.bazaarinterview.parisad.R;
import com.bazaarinterview.parisad.adapters.PlaceAdapter;
import com.bazaarinterview.parisad.bases.BaseVMActivity;
import com.bazaarinterview.parisad.databinding.ActivityMainBinding;
import com.bazaarinterview.parisad.dialogs.dialog_permission.DialogPermission;
import com.bazaarinterview.parisad.dialogs.dialog_permission.InterfaceDialogPermission;
import com.bazaarinterview.parisad.helper.Constant;
import com.bazaarinterview.parisad.helper.Utilities;
import com.bazaarinterview.parisad.interfaces.NavigatorInterface;
import com.bazaarinterview.parisad.models.PlaceModel;
import com.bazaarinterview.parisad.viewModels.MainActivityViewModel;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class MainActivity extends BaseVMActivity implements NavigatorInterface {
    public static Location location;
    MainActivityViewModel viewModel;
    ActivityMainBinding myBinding;
    PlaceAdapter myAdapter;
    Boolean isLoading;

    String[] PERMISSIONS = {Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION};
                          //  Manifest.permission.ACCESS_BACKGROUND_LOCATION };
    String[] PERMISSIONS1 = {Manifest.permission.ACCESS_FINE_LOCATION};
    String[] PERMISSIONS2={Manifest.permission.ACCESS_COARSE_LOCATION};
  //  String[] PERMISSIONS3={Manifest.permission.ACCESS_BACKGROUND_LOCATION};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = ViewModelProviders.of(this).get(MainActivityViewModel.class);
        myBinding = (ActivityMainBinding) binding(R.layout.activity_main, viewModel);

        viewModel.setNavigatorInterface(this);
        viewModel.check_gps(getApplicationContext());
        viewModel.check_net(getApplicationContext());
        if (!hasPermissions(this, PERMISSIONS)) {
            DialogPermission dialogPermission = new DialogPermission(
                    this,
                    interfaceDialogPermission);
            dialogPermission.showDialog();
        } else {
            try {
                    start();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        initRV();
        setObserver();

    }

    @Override
    protected void onResume() {
        super.onResume();
        viewModel.check_gps(getApplicationContext());
        viewModel.check_net(getApplicationContext());
    }
    //----------------------------------------------------

    //----------------------------------------------------
    public void setObserver() {
        viewModel.isGPS_clicked.observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if (aBoolean) {
                    new Utilities().goToSetting(getApplicationContext());
                }
            }
        });
        //-----------------
        viewModel.getUserMessage().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                Toast.makeText(getApplicationContext(),s,Toast.LENGTH_LONG).show();
            }
        });
        //-----------------
        viewModel.getPlaceList().observe(this, new Observer<List<PlaceModel>>() {
            @Override
            public void onChanged(List<PlaceModel> places) {
                //show in rv
                try {
                    myAdapter.setDataList(places);
                }catch(Exception ex){
                    String str=ex.getMessage();
                }
            }
        });
        //-----------------
    }

    //-----------------------------------------------
    public void start() throws ExecutionException, InterruptedException {
        viewModel.init(getApplicationContext());
    }
    //---------------
    public void requestPermission(){
        ActivityCompat.requestPermissions(MainActivity.this, PERMISSIONS1, 0);
    }
    //----------------------------------------------------------------------------------------------
    //check if permission is granted or not
    public boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }
    //----------------------------------------------------------------------------------------------
    //check result of permission
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 0 && permissions[0].equals(Manifest.permission.ACCESS_FINE_LOCATION)){
            switch (grantResults[0]){
                case PackageManager.PERMISSION_GRANTED:
                    ActivityCompat.requestPermissions(MainActivity.this, PERMISSIONS2, 1);
                    break;
                case PackageManager.PERMISSION_DENIED:
                    finish();
                    break;
            }
        }else if (requestCode == 1 && permissions[0].equals(Manifest.permission.ACCESS_COARSE_LOCATION)){
            switch (grantResults[0]){
                case PackageManager.PERMISSION_GRANTED:
                   // ActivityCompat.requestPermissions(MainActivity.this, PERMISSIONS3, 2);
                    try {
                        start();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    break;
                case PackageManager.PERMISSION_DENIED:
                    finish();
                    break;
            }
        }
//        else if (requestCode == 2 && permissions[0].equals(Manifest.permission.ACCESS_BACKGROUND_LOCATION)){
//            switch (grantResults[0]){
//                case PackageManager.PERMISSION_GRANTED:
//                    try {
//                        start();
//                    } catch (ExecutionException e) {
//                        e.printStackTrace();
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                    break;
//                case PackageManager.PERMISSION_DENIED:
//                    finish();
//                    break;
//            }
//        }

        else {
            //or show something
            finish();
        }

    }
    //--------------------------------------------------------
    public void initRV(){
        LinearLayoutManager linearLayoutManager
                = new LinearLayoutManager(myBinding.recyclerview.getContext(), LinearLayoutManager.VERTICAL, false);
        myAdapter=new PlaceAdapter(viewModel);
        myBinding.recyclerview.setHasFixedSize(true);
        myBinding.recyclerview.setLayoutManager(linearLayoutManager);
        myBinding.recyclerview.setItemAnimator(new DefaultItemAnimator());
        myBinding.recyclerview.setAdapter(myAdapter);
        myBinding.recyclerview.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                   viewModel.loadDataMore(linearLayoutManager.findLastVisibleItemPosition(),
                           getApplicationContext());
           }
        });
    }
   //-----------------------------------------------------------------------------------------------
    InterfaceDialogPermission interfaceDialogPermission=new InterfaceDialogPermission() {
       @Override
       public void startRequest(boolean result) {
           if (result){
               requestPermission();
           }else{
               finish();
           }
       }
   };

    @Override
    public void onItemClick(PlaceModel model) {
        Intent intent = new Intent(MainActivity.this, DetailActivity.class);
        intent.putExtra(Constant.EXTRA_ID, model.getId());
        startActivity(intent);
    }

}