package com.bazaarinterview.parisad.helper;

import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.Settings;

import java.io.IOException;
import java.text.DecimalFormat;

public class Utilities {
    public Utilities() {
    }
    public boolean isGPS_ON(Context context){
        LocationManager locationManager = (LocationManager)context.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public  boolean isOnline(Context context){
        ConnectivityManager mgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = mgr.getActiveNetworkInfo();
        if (netInfo != null) {
            if (netInfo.isConnected()) {
                return checkNet();
            }else {
                //No internet
                return false;
            }
        } else {
            //No internet
            return false;
        }
    }
    //----------------------------added by parisa
    public  boolean checkNet(){
        try {
            //--------------way2
            Process process = Runtime.getRuntime().exec("/system/bin/ping -c 1 8.8.8.8");
            ProcessWithTimeout processWithTimeout = new ProcessWithTimeout(process);
            int exitCode = processWithTimeout.waitForProcess(3000);

            if (exitCode == Integer.MIN_VALUE)
            {
                // Timeout
                return false;
            }
            else
            {
                // No timeout !
                return true;
            }
        } catch (IOException e)          {
            e.printStackTrace();
            return false;
        }
        catch (Exception e) { e.printStackTrace();
            return false;
        }
    }
    //------------------------------------------------------------------
    public void goToSetting(Context context){
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                                  .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }
    //-------------------------------------------------------------------
    public void openUrl(String url,Context context){
        if (url!=null && !url.isEmpty()) {
            Intent i = new Intent(Intent.ACTION_VIEW).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.setData(Uri.parse(url));
            context.startActivity(i);
        }
    }
    //--------------------------------------------------------------------
    //convert meter to kilometer
    public String meterTokilo(Double distance){
        double d = distance/1000;
        return new DecimalFormat("#.#").format(d);
    }

}
