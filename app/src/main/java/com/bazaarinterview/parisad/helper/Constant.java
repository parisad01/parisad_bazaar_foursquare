package com.bazaarinterview.parisad.helper;

public class Constant {
    public static final int CONTIMEOUT = 90000;
    public static final int SOCTIMEOUT = 60000;

    public static final String URL_GET_PLACES = "venues/explore?";
    public static final String URL_GET_DETAIL="venues/";
    public static final String URL_BASE="https://api.foursquare.com/v2/";
    //-----------------------------------------------------------------
    //Dao table names
    public static final String table_place="table_place";
    public static final String table_location="table_location";
    public static final String table_detail="table_detail";
    //-----------------------------------------------------------------
    //foursquare
    public static final String CLIENT_ID="ZND01C5OPZ12FP2NOFAZMO4XBRPDXBHSFT5A43VZJ4KL15UP";
    public static final String CLIENT_SECRET="M2CSDGYWMZZRR43JEFUDBRGN2RMPE2NJSDRVNBA4OTTKQQAO";
    public static final Integer v=20200101;//20180323
    //------------------------------------------------------------------
    // for compare
    public static final int END_TIME=1*24*60*60*1000;//1 days in milli seconds
    public static final double END_DISTANCE=100;
    //------------------------------------------------------------------
    //category icon
    public static final String ICON_SIZE_BG="bg_32";//bg_32
    //venue photo size
    public static final String DETAIL_PHOTO_SIZE="500x300";
    //main navigation intent extra
    public static final String EXTRA_ID="EXTRA_ID";
  //kilometer
    public static final String KILOMETER="کیلومتر";
    //empty url
    public static final String EMPTY_URL="این مکان وبسایت اختصاصی ندارد";

}
