package com.bazaarinterview.parisad.helper;


import android.content.Context;
import android.location.Location;
import android.location.LocationManager;

import com.bazaarinterview.parisad.activities.MainActivity;

import java.util.List;

public class GPS_listener{
    Location loc;
    public GPS_listener(Context context) {
        LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        List<String> providers = manager.getAllProviders();

        try {
            for (int i = 0; i < providers.size(); i++) {
                if (manager.getLastKnownLocation(providers.get(i))!=null)
                   loc = manager.getLastKnownLocation(providers.get(i));
            }
        }catch (SecurityException ex){}
    }
    public Location getMyLocation(){
        return loc;
    }
}
