package com.bazaarinterview.parisad.interfaces;

import com.bazaarinterview.parisad.models.PlaceModel;

public interface OpenUrlInterface {
    void openUrl(String url);
}
