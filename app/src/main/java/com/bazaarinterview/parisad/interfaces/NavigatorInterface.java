package com.bazaarinterview.parisad.interfaces;

import com.bazaarinterview.parisad.models.PlaceModel;

public interface NavigatorInterface {
    void onItemClick(PlaceModel model);
}
