package com.bazaarinterview.parisad.adapters;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bazaarinterview.parisad.R;
import com.bazaarinterview.parisad.databinding.LayoutPlaceItemBinding;
import com.bazaarinterview.parisad.models.PlaceModel;
import com.bazaarinterview.parisad.roomDatabase.Place.PlaceRoom;
import com.bazaarinterview.parisad.viewModels.MainActivityViewModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by Administrator on 3/3/2018.
 */

public class PlaceAdapter extends RecyclerView.Adapter<PlaceAdapter.MyViewHolder>
{
    private List<PlaceModel> AdapterList;
    private MainActivityViewModel viewModel;
    public PlaceAdapter(MainActivityViewModel vm) {
        this.viewModel=vm;
    }
    public void setDataList(List<PlaceModel> AdapterList){
        this.AdapterList=AdapterList;
        notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutPlaceItemBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.layout_place_item, parent, false);

        return new MyViewHolder(binding);

    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        PlaceModel model = AdapterList.get(position);
        try {
            Picasso.get().load(model.getCategory_icon()).fit().centerCrop()
                    .placeholder(R.drawable.ic_no_category)
                    .error(R.drawable.ic_no_category)
                    .into(holder.binding.categoryIcon);
        }catch (Exception ex){

        }

        holder.bind(model,position);

    }


    @Override
    public int getItemCount() {
        if (AdapterList!=null) {
            return AdapterList.size();
        }
        return 0;
    }
    @Override
    public int getItemViewType(int position) { return position; }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public LayoutPlaceItemBinding binding;

        public MyViewHolder(LayoutPlaceItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        private String[] getNames(PlaceModel model){
            return model.getName().split("\\|");
        }

        public void bind(Object obj, int index) {
            binding.setPosition(index);
            binding.setModel(AdapterList.get(index));
            binding.setViewModel(viewModel);
            String[] names=getNames(AdapterList.get(index));
            if (names!=null ) {
                binding.setNameEN((names.length >= 1) ? names[0] : "");
                binding.setNameFA((names.length >= 2) ? names[1] : "");
            }
            binding.executePendingBindings();
        }
    }

}



