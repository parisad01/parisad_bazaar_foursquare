package com.bazaarinterview.parisad.customViews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.annotation.Nullable;

public class MaterialTextView extends androidx.appcompat.widget.AppCompatTextView {
    public MaterialTextView(Context context) {
        super(context);
        init();
    }

    public MaterialTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();

    }

    public MaterialTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();

    }

    private void init()
    {
        Typeface typeface = Typeface.createFromAsset(getContext().getAssets(),"fonts/Material-Design-Iconic-Font.ttf");
        setTypeface(typeface);
    }
}