package com.bazaarinterview.parisad.bases;

import android.os.Bundle;
import android.view.View;


import com.bazaarinterview.parisad.BR;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.lifecycle.ViewModel;

public class BaseVMActivity extends BaseActivity {
    protected ViewDataBinding mBinding;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected ViewDataBinding binding(int layoutId, ViewModel viewModel) {
        mBinding = DataBindingUtil.setContentView(this, layoutId);
        mBinding.setVariable(BR.viewModel, viewModel);
        mBinding.setLifecycleOwner(this);
        return mBinding;
    }

    @Override
    protected void onResume(){
        super.onResume();
    }
    //--------------------------------------------------------------------------------------------------
   }
