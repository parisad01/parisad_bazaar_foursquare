package com.bazaarinterview.parisad.mappers;

import android.content.ClipData;
import android.graphics.ColorSpace;

import com.bazaarinterview.parisad.helper.Constant;
import com.bazaarinterview.parisad.models.Category;
import com.bazaarinterview.parisad.models.DetailModel;
import com.bazaarinterview.parisad.models.DetailRequestOutput;
import com.bazaarinterview.parisad.models.Icon;
import com.bazaarinterview.parisad.models.Item;
import com.bazaarinterview.parisad.models.Location;
import com.bazaarinterview.parisad.models.PlaceModel;
import com.bazaarinterview.parisad.models.RequestOutput;
import com.bazaarinterview.parisad.models.Venue;
import com.bazaarinterview.parisad.models.VenueDetail;
import com.bazaarinterview.parisad.roomDatabase.Detail.DetailRoom;
import com.bazaarinterview.parisad.roomDatabase.Place.PlaceRoom;

import java.util.ArrayList;
import java.util.List;

public class Mapper {
    public Mapper() {
    }

    public PlaceRoom mapToPlaceRoom(PlaceModel model){
        PlaceRoom placeRoom=new PlaceRoom();
        placeRoom.setName(model.getName());
        placeRoom.setAddress(model.getAddress());
        placeRoom.setCategory_name(model.getCategory_name());
        placeRoom.setCategory_icon(model.getCategory_icon());
        placeRoom.setCc(model.getCc());
        placeRoom.setCity(model.getCity());
        placeRoom.setCountry(model.getCountry());
        placeRoom.setDistance(model.getDistance());
        placeRoom.setId(model.getId());
        placeRoom.setLat(model.getLat());
        placeRoom.setLng(model.getLng());
        placeRoom.setState(model.getState());
        placeRoom.setTableId(model.getTableId());
        return placeRoom;
    }

    public PlaceModel mapToPlaceModel(PlaceRoom model){
        PlaceModel placeModel=new PlaceModel();
        placeModel.setName(model.getName());
        placeModel.setAddress(model.getAddress());
        placeModel.setCategory_name(model.getCategory_name());
        placeModel.setCategory_icon(model.getCategory_icon());
        placeModel.setCc(model.getCc());
        placeModel.setCity(model.getCity());
        placeModel.setCountry(model.getCountry());
        placeModel.setDistance(model.getDistance());
        placeModel.setId(model.getId());
        placeModel.setLat(model.getLat());
        placeModel.setLng(model.getLng());
        placeModel.setState(model.getState());
        placeModel.setTableId(model.getTableId());
        return placeModel;
    }

    public DetailModel mapToDetailModel(DetailRoom room){
        DetailModel model=new DetailModel();
        model.setName(room.getName());
        model.setAddress(room.getAddress());
        model.setCategory_name(room.getCategory_name());
        model.setCategory_icon(room.getCategory_icon());
        model.setCc(room.getCc());
        model.setCity(room.getCity());
        model.setCountry(room.getCountry());
        model.setDistance(room.getDistance());
        model.setId(room.getId());
        model.setLat(room.getLat());
        model.setLng(room.getLng());
        model.setState(room.getState());
        model.setTableId(room.getTableId());
        model.setCanonicalUrl(room.getCanonicalUrl());
        model.setUrl(room.getUrl());
        model.setPhotoUrl(room.getPhotoUrl());
        return model;
    }

    public DetailRoom mapToDetailRoom(DetailModel model){
        DetailRoom room=new DetailRoom();
        room.setName(model.getName());
        room.setAddress(model.getAddress());
        room.setCategory_name(model.getCategory_name());
        room.setCategory_icon(model.getCategory_icon());
        room.setCc(model.getCc());
        room.setCity(model.getCity());
        room.setCountry(model.getCountry());
        room.setDistance(model.getDistance());
        room.setId(model.getId());
        room.setLat(model.getLat());
        room.setLng(model.getLng());
        room.setState(model.getState());
        room.setTableId(model.getTableId());
        room.setCanonicalUrl(model.getCanonicalUrl());
        room.setUrl(model.getUrl());
        room.setPhotoUrl(model.getPhotoUrl());
        return room;
    }

    public DetailModel PlaceRoomToDetailModel(PlaceRoom room){
        DetailModel model=new DetailModel();
        model.setName(room.getName());
        model.setAddress(room.getAddress());
        model.setCategory_name(room.getCategory_name());
        model.setCategory_icon(room.getCategory_icon());
        model.setCc(room.getCc());
        model.setCity(room.getCity());
        model.setCountry(room.getCountry());
        model.setDistance(room.getDistance());
        model.setId(room.getId());
        model.setLat(room.getLat());
        model.setLng(room.getLng());
        model.setState(room.getState());
        model.setTableId(room.getTableId());

        return model;
    }

    public DetailModel ReqOutToDetailModel(DetailRequestOutput output){
        DetailModel model=new DetailModel();
        VenueDetail venue=output.getResponse().getVenue();
        if (venue!=null) {
            model.setUrl(venue.getUrl());
            model.setCanonicalUrl(venue.getCanonicalUrl());
            model.setName(venue.getName());
            model.setId(venue.getId());
            Icon icon=venue.getBestPhoto();
            if (icon!=null){
                model.setPhotoUrl(icon.getPrefix()+
                        Constant.DETAIL_PHOTO_SIZE+
                        icon.getSuffix());
            }

            Location location=venue.getLocation();
            if (location!=null) {
                model.setAddress(location.getAddress());
                model.setCc(location.getCc());
                model.setCity(location.getCity());
                model.setCountry(location.getCountry());
                model.setDistance(location.getDistance());
                model.setLat(location.getLat());
                model.setLng(location.getLng());
                model.setState(location.getState());
            }
            Category category=(venue.getCategories()!=null)?venue.getCategories().get(0):null;
            if (category!=null){
                model.setCategory_name(category.getName());
                model.setCategory_icon(category.getIcon().getPrefix()+
                        Constant.ICON_SIZE_BG +
                        category.getIcon().getSuffix());
            }
        }
        return model;
    }

    public List<PlaceModel> ReqOutToPlaceModel(RequestOutput model){
        List<PlaceModel> placeList=new ArrayList<>();
        List<Item> items=model.getResponse().getGroups().get(0).getItems();
        for (Item item:items){
            PlaceModel pm=new PlaceModel();
            Venue venue=item.getVenue();
            pm.setId(venue.getId());
            pm.setName(venue.getName());
            Location location=venue.getLocation();
            pm.setAddress(location.getAddress());
            pm.setLat(location.getLat());
            pm.setLng(location.getLng());
            pm.setDistance(location.getDistance());
            pm.setCc(location.getCc());
            pm.setCity(location.getCity());
            pm.setState(location.getState());
            pm.setCountry(location.getCountry());
            Category category=venue.getCategories().get(0);
            pm.setCategory_name(category.getName());
            pm.setCategory_icon(category.getIcon().getPrefix()+
                                 Constant.ICON_SIZE_BG +
                                category.getIcon().getSuffix());
            placeList.add(pm);
        }
        return placeList;
    }

    public List<PlaceModel> mapToModel_list(List<PlaceRoom> list){
        List<PlaceModel> placeModels=new ArrayList<>();
        for (PlaceRoom pr :list){
            placeModels.add(mapToPlaceModel(pr));
        }
            return placeModels;
    }

    public List<PlaceRoom> mapToEntity_list(List<PlaceModel> list){
        List<PlaceRoom> placeRooms=new ArrayList<>();
        for (PlaceModel pm :list){
            placeRooms.add(mapToPlaceRoom(pm));
        }
        return placeRooms;
    }
}
