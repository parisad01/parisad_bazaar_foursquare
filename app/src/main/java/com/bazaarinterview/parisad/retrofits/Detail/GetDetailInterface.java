package com.bazaarinterview.parisad.retrofits.Detail;

import com.bazaarinterview.parisad.helper.Constant;
import com.bazaarinterview.parisad.models.DetailRequestOutput;
import com.bazaarinterview.parisad.models.RequestOutput;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface GetDetailInterface {
    @GET(Constant.URL_GET_DETAIL+"/{id}")
    Call<DetailRequestOutput> getDetail(@Path("id") String venueId,
                                        @Query("client_id") String client_id,
                                        @Query("client_secret") String client_secret,
                                        @Query("v") Integer v,
                                        @Query("ll") String ll,
                                        @Query("query") String query);
}
