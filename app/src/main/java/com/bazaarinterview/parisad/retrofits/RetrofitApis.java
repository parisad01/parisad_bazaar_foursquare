package com.bazaarinterview.parisad.retrofits;

import com.bazaarinterview.parisad.models.DetailRequestOutput;
import com.bazaarinterview.parisad.models.RequestOutput;
import com.bazaarinterview.parisad.retrofits.Detail.GetDetailInterface;
import com.bazaarinterview.parisad.retrofits.Detail.GetDetailResponseInterface;
import com.bazaarinterview.parisad.retrofits.Place.GetPlaceResponseInterface;
import com.bazaarinterview.parisad.retrofits.Place.GetPlacesInterface;

import java.util.concurrent.ExecutionException;


import retrofit2.Call;
import retrofit2.Callback;

import retrofit2.Response;

public class RetrofitApis {
    public RetrofitApis() {
    }

    //----------------------------------------------------------------------------------------------
    public void getPlaces(GetPlaceResponseInterface mInterface, String client_id
                          , String client_secret, Integer v, Integer limit,
                          Integer offset, String ll, String query) throws ExecutionException, InterruptedException {

        try {
            GetPlacesInterface getPlacesInterface = APIClient.getClient().create(GetPlacesInterface.class);

            Call<RequestOutput> call1 = getPlacesInterface.getPlace(
                    client_id, client_secret, v, limit, offset, ll, query);

            String str = call1.request().url().toString();
            call1.enqueue(new Callback<RequestOutput>() {
                @Override
                public void onResponse(Call<RequestOutput> call, Response<RequestOutput> response) {
                    try {
                        String s = response.toString();
                        if (response != null && response.code() == 200) {
                            RequestOutput requestOutput = response.body();
                            mInterface.updateUI(true, "", requestOutput);

                        } else {
                            mInterface.updateUI(false, "داده ای دریافت نشد", null);
                        }
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<RequestOutput> call, Throwable t) {
                    try {
                        mInterface.updateUI(false, "failure : " + t.getMessage(), null);
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
        }catch (Exception ex){
            mInterface.updateUI(false,"getPlaces + RetrofitApis",null);
        }
    }
    //----------------------------------------------------------------------------------------------
    public void getDetail(GetDetailResponseInterface mInterface,String venueId, String client_id
            , String client_secret, Integer v, String ll, String query){
        GetDetailInterface getDetailInterface = APIClient.getClient().create(GetDetailInterface.class);

        Call<DetailRequestOutput> call1=getDetailInterface.getDetail(venueId,
                client_id,client_secret,v,ll,query) ;

        String str=call1.request().url().toString();
        call1.enqueue(new Callback<DetailRequestOutput>() {
            @Override
            public void onResponse(Call<DetailRequestOutput> call, Response<DetailRequestOutput> response) {
                try {
                    String s = response.toString();
                    if (response != null && response.code() == 200) {
                        DetailRequestOutput requestOutput = response.body();
                        mInterface.updateUI(true, "", requestOutput);

                    } else {
                        if (response!=null && response.code()==429)
                            mInterface.updateUI(false, "محدودیت درخواست از سرور", null);
                        mInterface.updateUI(false, "داده ای دریافت نشد", null);
                    }
                }
                catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<DetailRequestOutput> call, Throwable t) {
                try {
                    mInterface.updateUI(false,"failure : "+t.getMessage(),null);
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }
    //----------------------------------------------------------------------------------------------

}
