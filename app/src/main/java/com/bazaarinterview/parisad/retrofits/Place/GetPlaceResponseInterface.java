package com.bazaarinterview.parisad.retrofits.Place;

import com.bazaarinterview.parisad.models.RequestOutput;

import java.util.concurrent.ExecutionException;

public interface GetPlaceResponseInterface {
    public void updateUI(boolean response, String str
            , RequestOutput requestOutput) throws ExecutionException, InterruptedException;
}
