package com.bazaarinterview.parisad.retrofits.Place;

import com.bazaarinterview.parisad.helper.Constant;
import com.bazaarinterview.parisad.models.RequestOutput;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GetPlacesInterface {
 //   @Headers("Content-Type: application/json;charset=UTF-8")
    @GET(Constant.URL_GET_PLACES)

    Call<RequestOutput> getPlace(@Query("client_id") String client_id,
                                 @Query("client_secret") String client_secret,
                                 @Query("v") Integer v,
                                 @Query("limit") Integer limit,
                                 @Query("offset") Integer offset,
                                 @Query("ll") String ll,
                                 @Query("query") String query);
}
