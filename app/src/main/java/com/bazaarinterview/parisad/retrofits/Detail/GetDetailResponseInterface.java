package com.bazaarinterview.parisad.retrofits.Detail;

import com.bazaarinterview.parisad.models.DetailRequestOutput;
import com.bazaarinterview.parisad.models.RequestOutput;

import java.util.concurrent.ExecutionException;

public interface GetDetailResponseInterface {
    public void updateUI(boolean response, String str
            , DetailRequestOutput detailOutput) throws
            ExecutionException, InterruptedException;
}
