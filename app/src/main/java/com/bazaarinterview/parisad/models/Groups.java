package com.bazaarinterview.parisad.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Groups {
    @SerializedName("items")
    private List<Item> Items;

    public Groups(List<Item> Items) {
        this.Items = Items;
    }

    public List<Item> getItems() {
        return Items;
    }

    public void setItems(List<Item> Items) {
        this.Items = Items;
    }
}
