package com.bazaarinterview.parisad.models;

import com.google.gson.annotations.SerializedName;

public class RequestOutput {
    @SerializedName("response")
    private Response response;
    @SerializedName("meta")
    private Meta meta;

    public RequestOutput(Response response) {

        this.response = response;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }
}
