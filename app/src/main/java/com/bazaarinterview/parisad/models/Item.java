package com.bazaarinterview.parisad.models;

import com.google.gson.annotations.SerializedName;

public class Item {
    @SerializedName("venue")
    private Venue venue;

    public Item(Venue venue) {
        this.venue = venue;
    }

    public Venue getVenue() {
        return venue;
    }

    public void setVenue(Venue venue) {
        this.venue = venue;
    }
}
