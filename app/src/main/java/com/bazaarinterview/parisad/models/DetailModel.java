package com.bazaarinterview.parisad.models;
//************this model is used for both retrofit and room*************//

import com.bazaarinterview.parisad.helper.Constant;
import com.bazaarinterview.parisad.roomDatabase.Place.PlaceRoom;

import androidx.room.ColumnInfo;
import androidx.room.Entity;


public class DetailModel extends PlaceModel {
     private String canonicalUrl;
     private String url;
     private String photoUrl;

    public String getCanonicalUrl() {
        return canonicalUrl;
    }

    public void setCanonicalUrl(String canonicalUrl) {
        this.canonicalUrl = canonicalUrl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }
}
