package com.bazaarinterview.parisad.models;

public class QueryResult {
    private Response response;

    public QueryResult(Response response) {
        this.response = response;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }
}
