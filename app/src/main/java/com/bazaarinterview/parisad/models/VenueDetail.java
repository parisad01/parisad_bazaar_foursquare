package com.bazaarinterview.parisad.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class VenueDetail extends Venue {
    @SerializedName("canonicalUrl")
    private String canonicalUrl;
    @SerializedName("url")
    private String url;
    @SerializedName("bestPhoto")
    private Icon bestPhoto;

    public String getCanonicalUrl() {
        return canonicalUrl;
    }

    public void setCanonicalUrl(String canonicalUrl) {
        this.canonicalUrl = canonicalUrl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Icon getBestPhoto() {
        return bestPhoto;
    }

    public void setBestPhoto(Icon bestPhoto) {
        this.bestPhoto = bestPhoto;
    }
}
