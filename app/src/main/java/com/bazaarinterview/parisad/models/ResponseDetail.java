package com.bazaarinterview.parisad.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseDetail {
   @SerializedName("venue")
   private VenueDetail venue;

    public VenueDetail getVenue() {
        return venue;
    }

    public void setVenue(VenueDetail venue) {
        this.venue = venue;
    }
}
