package com.bazaarinterview.parisad.models;

import com.google.gson.annotations.SerializedName;

public class RequestInput {
    @SerializedName("client_id")
    private String client_id;

    @SerializedName("client_secret")
    private String client_secret;

    @SerializedName("v")
    private Long v;

    @SerializedName("limit")
    private Integer limit;

    @SerializedName("ll")
    private String ll;

    @SerializedName("query")
    private String query;

    public RequestInput(String client_id, String client_secret,
                        Long v, Integer limit, String ll, String query) {
        this.client_id = client_id;
        this.client_secret = client_secret;
        this.v = v;
        this.limit = limit;
        this.ll = ll;
        this.query = query;
    }

    public String getClient_id() {
        return client_id;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    public String getClient_secret() {
        return client_secret;
    }

    public void setClient_secret(String client_secret) {
        this.client_secret = client_secret;
    }

    public Long getV() {
        return v;
    }

    public void setV(Long v) {
        this.v = v;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public String getLl() {
        return ll;
    }

    public void setLl(String ll) {
        this.ll = ll;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }
}
