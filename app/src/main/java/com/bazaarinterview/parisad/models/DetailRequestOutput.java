package com.bazaarinterview.parisad.models;

import com.google.gson.annotations.SerializedName;

public class DetailRequestOutput {
    @SerializedName("response")
    private ResponseDetail response;

    public ResponseDetail getResponse() {
        return response;
    }

    public void setResponse(ResponseDetail response) {
        this.response = response;
    }
}
