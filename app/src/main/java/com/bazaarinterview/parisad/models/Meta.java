package com.bazaarinterview.parisad.models;

import com.google.gson.annotations.SerializedName;

public class Meta {
    @SerializedName("code")
    private int code;
    @SerializedName("requestId")
    private String requestId;

    public Meta(int code, String requestId) {
        this.code = code;
        this.requestId = requestId;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }
}
