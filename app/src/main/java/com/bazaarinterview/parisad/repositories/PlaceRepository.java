package com.bazaarinterview.parisad.repositories;


import com.bazaarinterview.parisad.retrofits.Place.GetPlaceResponseInterface;
import com.bazaarinterview.parisad.retrofits.RetrofitApis;

public class PlaceRepository {
    private static PlaceRepository instance;
  // private static GetPlaceResponseInterface responseInterface;
    private static RetrofitApis retrofitApis;
    public static PlaceRepository getInstance(){
      //  responseInterface=response;
        if (instance==null){
            instance=new PlaceRepository();
            retrofitApis=new RetrofitApis();
        }
        return instance;
    }
   // ----------------------------------------------------------------------------------------------
    public void getPlaces(GetPlaceResponseInterface response,String client_id
            , String client_secret,Integer v,Integer limit,Integer offset,
                          String ll,String query){
        try {
            retrofitApis.getPlaces(response,client_id,client_secret,
                                   v,limit,offset,ll,query);
        }catch(Exception ex){

        }
    }
    //----------------------------------------------------------------------------------------------

}
