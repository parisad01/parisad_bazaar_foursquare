package com.bazaarinterview.parisad.repositories;


import com.bazaarinterview.parisad.retrofits.Detail.GetDetailResponseInterface;
import com.bazaarinterview.parisad.retrofits.Place.GetPlaceResponseInterface;
import com.bazaarinterview.parisad.retrofits.RetrofitApis;

public class DetailRepository {
    private static DetailRepository instance;
    private static RetrofitApis retrofitApis;
    public static DetailRepository getInstance(){
        if (instance==null){
            instance=new DetailRepository();
            retrofitApis=new RetrofitApis();
        }
        return instance;
    }
   // ----------------------------------------------------------------------------------------------
    public void getDetail(GetDetailResponseInterface response,String venueId, String client_id
            , String client_secret, Integer v,
                          String ll, String query){
        try {
            retrofitApis.getDetail(response,venueId,client_id,client_secret,
                                   v,ll,query);
        }catch(Exception ex){

        }
    }
    //----------------------------------------------------------------------------------------------

}
